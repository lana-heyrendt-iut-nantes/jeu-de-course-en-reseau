/*
// Implementation of a main function setting a few characteristics of
// the game window, creating a game, and launching it
*/

package main

import (
	"Network"
	"flag"
	"github.com/hajimehoshi/ebiten/v2"
	_ "image/png"
	"log"
	"strconv"
)

const (
	screenWidth  = 800 // Width of the game window (in pixels)
	screenHeight = 160 // Height of the game window (in pixels)
)

var debug bool

func main() {

	var getTPS bool
	// Variable stockant l'adresse du serveur de jeu
	var servAddr string

	flag.BoolVar(&getTPS, "tps", false, "Afficher le nombre d'appel à Update par seconde")
	flag.BoolVar(&debug, "debug", false, "Log les informations de debug")
	flag.StringVar(&servAddr, "addr", "localhost", "precise the serveur adress")
	flag.Parse()
	ebiten.SetWindowSize(screenWidth, screenHeight)
	ebiten.SetWindowTitle("BUT2 année 2022-2023, R3.05 Programmation système")

	g := InitGame()
	g.getTPS = getTPS

	if debug {
		log.Println("Connecting to : ", servAddr)
		debugLogState = 0
	}
	g.cliTool = Network.NewClient(servAddr)

	go g.cliTool.Lecture()
	go g.cliTool.Ecriture()

	playerId := <-g.cliTool.CanalLecture
	log.Println("Player ID :", playerId)

	var PlayerIdErr error
	g.playerID, PlayerIdErr = strconv.Atoi(playerId)
	if PlayerIdErr != nil {
		log.Fatal("Invalid ID:", PlayerIdErr)
	}

	err := ebiten.RunGame(&g)
	log.Print(err)

}
