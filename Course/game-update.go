/*
//  Implementation of the Update method for the Game structure
//  This method is called once at every frame (60 frames per second)
//  by ebiten, juste before calling the Draw method (game-draw.go).
//  Provided with a few utilitary methods:
//    - CheckArrival
//    - ChooseRunners
//    - HandleLaunchRun
//    - HandleResults
//    - HandleWelcomeScreen
//    - Reset
//    - UpdateAnimation
//    - UpdateRunners
*/

package main

import (
	"log"
	"strconv"
	"time"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
)

var debugLogState int = -1

// HandleWelcomeScreen waits for the player to push SPACE in order to
// start the game

func (g *Game) HandleWelcomeScreen() bool {
	select {
	case msg := <-g.cliTool.CanalLecture:
		log.Println(msg)
		g.connected = g.cliTool.GetNumberOfPlayerConnected(msg)
		if g.connected == len(g.runners) {
			g.gameLatch = true
		}
	default:
	}
	return inpututil.IsKeyJustPressed(ebiten.KeySpace) && g.gameLatch
}

// ChooseRunners loops over all the runners to check which sprite each
// of them selected
func (g *Game) ChooseRunners() bool {
	if !g.gameLatch && g.runners[g.playerID-1].ManualChoose() {
		g.gameLatch = true
		colorS := strconv.Itoa(g.runners[g.playerID-1].colorScheme)
		g.cliTool.CanalEcriture <- colorS
		if debug {
			log.Println("Color scheme", colorS, "selected")
		}
	}
	select {
	case m := <-g.cliTool.CanalLecture:
		playersColorScheme := g.cliTool.ParseMessageInt(m)
		for i := range playersColorScheme {
			if i != g.playerID-1 {
				g.runners[i].colorScheme = playersColorScheme[i]
			}
		}
		if debug {
			log.Println("All player selected a color scheme")
		}
		return true
	default:
		return false
	}
}

// HandleLaunchRun countdowns to the start of a run
func (g *Game) HandleLaunchRun() bool {
	if time.Since(g.f.chrono).Milliseconds() > 1000 {
		g.launchStep++
		g.f.chrono = time.Now()
	}
	if g.launchStep >= 5 {
		g.launchStep = 0
		return true
	}
	return false
}

// UpdateRunners loops over all the runners to update each of them
func (g *Game) UpdateRunners() string {
	g.runners[g.playerID-1].ManualUpdate()
	if !g.gameLatch {
		speed := strconv.FormatFloat(g.runners[g.playerID-1].speed, 'f', -1, 64)
		g.cliTool.CanalEcriture <- speed
	}

	select {
	case msg := <-g.cliTool.CanalLecture:
		if debug {
			log.Println(msg)
		}
		if msg == "Arrived;Arrived;Arrived;Arrived" {
			if debug {
				log.Println("All player have arrived")
			}
			return "End"
		} else {
			speedTab := g.cliTool.ParsePlayersSpeed(msg)
			for i := range g.runners {
				if i != g.playerID-1 {
					g.runners[i].ParametrizedUpdate(speedTab[i])
				}
			}
		}
	default:
	}
	return ""
}

// CheckArrival loops over all the runners to check which ones are arrived
func (g *Game) CheckArrival(msg string) bool {
	for i := range g.runners {
		g.runners[i].CheckArrival(&g.f)
	}
	if !g.gameLatch && g.runners[g.playerID-1].arrived {
		g.cliTool.CanalEcriture <- "Arrived"
		g.gameLatch = true
		g.cliTool.CanalEcriture <- g.runners[g.playerID-1].runTime.String()
	}
	return msg == "End"
}

// Reset resets all the runners and the field in order to start a new run
func (g *Game) Reset() {
	if debug {
		log.Println("checking gameLatch")
	}
	for i := range g.runners {
		g.runners[i].Reset(&g.f)
	}
	g.f.Reset()
}

// UpdateAnimation loops over all the runners to update their sprite
func (g *Game) UpdateAnimation() {
	for i := range g.runners {
		g.runners[i].UpdateAnimation(g.runnerImage)
	}
}

func (g *Game) getPlayersTime() bool {
	if !g.gameLatch {
		select {
		case msg := <-g.cliTool.CanalLecture:
			playersTimes := g.cliTool.ParseMessageTime(msg)
			for i := range g.runners {
				if i != g.playerID-1 {
					g.runners[i].runTime = playersTimes[i]
				}
			}
			g.gameLatch = true
		default:
		}
	}
	return g.gameLatch
}

// HandleResults computes the resuls of a run and prepare them for
// being displayed
func (g *Game) HandleResults() bool {
	if time.Since(g.f.chrono).Milliseconds() > 1000 || inpututil.IsKeyJustPressed(ebiten.KeySpace) {
		g.resultStep++
		g.f.chrono = time.Now()
	}
	if g.resultStep >= 4 && inpututil.IsKeyJustPressed(ebiten.KeySpace) {
		g.resultStep = 0
		if g.gameLatch {
			g.gameLatch = false
			g.cliTool.CanalEcriture <- "true"
		}
		return true
	}
	return false
}

// Update is the main update function of the game. It is called by ebiten
// at each frame (60 times per second) just before calling Draw (game-draw.go)
// Depending of the current state of the game it calls the above utilitary
// function and then it may update the state of the game
func (g *Game) Update() error {
	switch g.state {
	case StateWelcomeScreen:
		done := g.HandleWelcomeScreen()
		if done {
			g.state++
			g.gameLatch = false
		}
	case StateChooseRunner:
		done := g.ChooseRunners()
		if done {
			g.UpdateAnimation()
			g.state++
		}
	case StateLaunchRun:
		done := g.HandleLaunchRun()
		if done {
			g.state++
			g.gameLatch = false
		}
	case StateRun:
		m := g.UpdateRunners()
		finished := g.CheckArrival(m)
		g.UpdateAnimation()
		if finished {
			g.gameLatch = false
			g.state++
		}
	case StateResult:
		done := g.getPlayersTime()
		if done {
			done = g.HandleResults()
		}
		if done {
			g.gameLatch = false
			g.state++
		}
	case StateRestart:
		select {
		case <-g.cliTool.CanalLecture:
			g.Reset()
			g.state = StateLaunchRun
		default:
		}
	}
	if debugLogState != g.state && debugLogState != -1 {
		log.Println("Player", g.playerID, "curent game state :", g.state)
		debugLogState = g.state
	}
	return nil
}
