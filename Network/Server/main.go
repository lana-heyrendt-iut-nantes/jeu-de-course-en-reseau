package main

import (
	"flag"
	"log"
	"net"
	"strconv"
	"strings"
	"sync"
)

var Players []Player  // Player tab
var wG sync.WaitGroup // WaitGroup used to wait for all the players
var State []chan int  // State channel

var debugLogState int = -1 // Debugging tool for States
var debugLogBroadcast bool // Debugging tool for Broadcast

const (
	StateInit                 int = iota // First state of the game (Welcome Screen)
	StateColorSchemeSelection            // Second state of the game (Color Scheme selection)
	StateRun                             // Third state : The game is running, all players are playing
	StateArrival                         // Fourth state : Every players have arrived
	StateWaitingForRestart               // Fifth state : Waiting for players to restart
)

func init() {
	// Listening incoming connections
	lis, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatalln(err)
	}

	log.Println("Waiting for Players")

	var conn net.Conn
	// For each player, Accept their connection
	for i := 0; len(Players) < 4; i++ {
		conn, err = lis.Accept()
		if err != nil {
			log.Println("Accept error :", err)
		} else {
			// Create Player list with their own connection
			Players = append(Players, newPlayer(conn, i+1))
			// Attributing ID to player and sending them
			Players[len(Players)-1].canalEcriture <- strconv.Itoa(i + 1)
			// Create the State channel for each player
			State = append(State, make(chan int, 1))
			// Writes true to indicate that the server is initialized
			for i := range Players {
				Players[i].canalInterne <- "true"
			}
			// Sends true to all players
			broadcastInternalChan()
		}
	}
	// Create the state channel for the server
	State = append(State, make(chan int, 1))
	// Parse debug option
	var flagDebugLogState = flag.Bool("logState", false, "Log when server is switching state")
	var flagDebugLogBroadcast = flag.Bool("logBroadcast", false, "Log when server is broadcasting a message to all players")
	flag.Parse()
	if *flagDebugLogState {
		debugLogState = 0
	}
	debugLogBroadcast = *flagDebugLogBroadcast
}

// UpdateState updates the server state
func UpdateState(state int) {
	for i := 0; i < len(Players)+1; i++ {
		State[i] <- state
	}
	//log.Println("Updating state to :", state)
	if debugLogState != state && debugLogState != -1 {
		debugLogState = state
		log.Println("Updating state to :", state)
	}

}

// broadcastInternalChan sends to all players what's in their channel
func broadcastInternalChan() {
	var msg string = ""
	for i := range Players {
		msg += <-Players[i].canalInterne + ";"
	}
	msg = strings.TrimSuffix(msg, ";")
	if debugLogBroadcast {
		log.Println(msg)
	}
	for i := range Players {
		Players[i].canalEcriture <- msg
	}
}

// SelectColorScheme updates color schemes chosen by players
func SelectColorScheme(p *Player, colorS int) bool {
	for i := range Players {
		if Players[i].colorScheme == colorS && Players[i].playerID != p.playerID {
			return false
		}
	}
	p.colorScheme = colorS
	return true
}

// checkArrival checks if every players have arrived
func checkArrival() bool {
	var finished = true
	for i := range Players {
		finished = finished && Players[i].arrived
	}
	return finished
}

// serverRoutine : Main Server Routine
func serverRoutine(state *chan int) {
	// Waiting for each Player to finish initializing
	wG.Add(4)
	// Next state
	UpdateState(StateInit)
	for {
		switch <-*state {
		// Waiting for each Player to connect to the server
		case StateInit:
			wG.Wait()
			log.Println("All player are connected")
			wG.Add(4)
			UpdateState(StateColorSchemeSelection)
		// Waitng for each Player to select a color scheme and sends all selected color schemes to every player
		case StateColorSchemeSelection:
			wG.Wait()
			broadcastInternalChan()
			UpdateState(StateRun)
		// Check if every player has arrived, if they haven't, restart the loop, in the StateRun

		case StateRun:
			if !checkArrival() {
				broadcastInternalChan()
				UpdateState(StateRun)
			} else {
				broadcastInternalChan()
				wG.Add(4)
				UpdateState(StateArrival)
			}
		// If they have, tells all players that everybody has arrived
		case StateArrival:
			wG.Wait()
			broadcastInternalChan()
			wG.Add(4)

			UpdateState(StateArrival)
			<-*state
			wG.Wait()
			broadcastInternalChan()
			wG.Add(4)

			UpdateState(StateWaitingForRestart)
		// Waits for 4 players to ask for a restart and goes bacj to the StateRun
		case StateWaitingForRestart:
			wG.Wait()
			broadcastInternalChan()
			UpdateState(StateRun)
		}
	}
}

func main() {
	for i := range Players {
		go Players[i].playerRoutine(&State[i])
	}
	serverRoutine(&State[len(Players)])
}
