package main

import (
	"Network"
	"log"
	"net"
)

type Player struct {
	canalLecture  chan string // chanel designed to read player messages
	canalEcriture chan string // chanel designed to write to the player
	canalInterne  chan string // chanel designed to interact with the server within a player routine
	playerID      int         // ID of the player
	arrived       bool        // Whether the player has arrived or not
	colorScheme   int         // Color Scheme Selected by the player
}

// Create a new Player
func newPlayer(conn net.Conn, id int) Player {
	var p Player
	ctk := Network.NewConnectionToolkit(conn)
	p.canalLecture = make(chan string, 1)
	p.canalEcriture = make(chan string, 1)
	p.canalInterne = make(chan string, 1)
	p.playerID = id
	p.arrived = false
	p.colorScheme = -1
	go p.read(ctk)
	go p.write(ctk)
	return p
}

// Reads messages sent by the client
func (p *Player) read(ctk Network.ConnectionToolkit) {
	for {
		p.canalLecture <- ctk.ReceiveMessage()
	}
}

// Writes messages and send them to the client
func (p *Player) write(ctk Network.ConnectionToolkit) {
	for {
		ctk.SendMessage(<-p.canalEcriture)
	}
}

// Main Player Routien
func (p *Player) playerRoutine(state *chan int) {
	for {
		// Checks the game State
		switch <-*state {
		// tells the wG that every Player connected is initialized and ready to go to the next state
		case StateInit:
			wG.Done()
		//
		case StateColorSchemeSelection:
			// Read color scheme sent by the player
			var colorScheme string = <-p.canalLecture
			log.Println("Player", p.playerID, "as selected the color scheme number", colorScheme)
			// Store selected colorScheme
			p.canalInterne <- colorScheme
			wG.Done()
		case StateRun:
			// Checks if player has sent the message "Arrived" in which case, his speed is set to 0
			if !p.arrived {
				msg := <-p.canalLecture
				if msg == "Arrived" {
					p.arrived = true
					msg = "0"
				}
				p.canalInterne <- msg
				// If the player doesn't send a message, sets his speed to 0
			} else {
				p.canalInterne <- "0"
			}
		case StateArrival:
			// Reset p.arrived in order to prepare for a potential restart
			p.arrived = false
			// Prepare to tell all player that everybody has arrived
			p.canalInterne <- "Arrived"
			// Tells the server to send the message
			wG.Done()
			// Waits for the server sending the messaged
			<-*state
			// Put the player time into the internal chanel to prepare for broadcast
			p.canalInterne <- <-p.canalLecture
			// Tells the server to send the message
			wG.Done()
		case StateWaitingForRestart:
			log.Println("Waiting for restart")
			// Waiting for the player to ask for a restart
			p.canalInterne <- <-p.canalLecture
			log.Println("Player", p.playerID, "asked for a restart")
			// Tells the Server that the player wants to restart
			wG.Done()
		}
	}
}
