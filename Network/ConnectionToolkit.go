package Network

import (
	"bufio"
	"log"
	"net"
	"strings"
)

// Struct containing, a net.Conn, a reader and a writer to allow
// sending and receiving messages

type ConnectionToolkit struct {
	conn   net.Conn
	reader *bufio.Reader
	writer *bufio.Writer
}

// Initialize the connection toolkit

func NewConnectionToolkit(co net.Conn) ConnectionToolkit {
	var ctk ConnectionToolkit
	ctk.conn = co
	ctk.reader = bufio.NewReader(ctk.conn)
	ctk.writer = bufio.NewWriter(ctk.conn)
	return ctk
}

// Sends a message
func (ctk ConnectionToolkit) SendMessage(message string) {
	_, err := ctk.writer.WriteString(message + "\n")
	if err != nil {
		log.Fatal("Writer error :", err)
	}
	err = ctk.writer.Flush()
	if err != nil {
		log.Fatal("Flush error :", err)
	}
}

// Receive a message
func (ctk ConnectionToolkit) ReceiveMessage() string {
	message, err := ctk.reader.ReadString('\n')
	if err != nil {
		log.Fatal(err)
	}
	return strings.Replace(message, "\n", "", -1)
}
