package Network

import (
	"log"
	"net"
	"strconv"
	"strings"
	"time"
)

// Struct with a connectionToolkit as well as 2 chanels in which we store written and received messages

type ClientTool struct {
	ctk           ConnectionToolkit
	CanalLecture  chan string
	CanalEcriture chan string
}

// Initialize a new ClientTool
func NewClient(addr string) ClientTool {
	conn, err := net.Dial("tcp", addr+":8080")
	if err != nil {
		log.Fatal("Dial error:", err)
	}
	var cli ClientTool
	cli.ctk = NewConnectionToolkit(conn)
	cli.CanalLecture = make(chan string, 1)
	cli.CanalEcriture = make(chan string, 1)
	return cli
}

// Tries to read the buffer and stores the message it in the channel
func (c ClientTool) Lecture() {
	for {
		c.CanalLecture <- c.ctk.ReceiveMessage()
	}
}

// Tries to read the CanalEcriture chanel and sends it to the Client
func (c ClientTool) Ecriture() {
	for {
		c.ctk.SendMessage(<-c.CanalEcriture)
	}
}

// ParseMessageInt : Parse int from a message and return them in a slice
func (c ClientTool) ParseMessageInt(msg string) []int {
	messageTab := strings.Split(msg, ";")
	var err error
	var tab = make([]int, 4)
	for i := range messageTab {
		tab[i], err = strconv.Atoi(messageTab[i])
		if err != nil {
			log.Fatal("parse error :", err)
		}
	}
	return tab
}

// ParseMessageTime : Parse durations from a message and return them in a slice
func (c ClientTool) ParseMessageTime(msg string) []time.Duration {
	messageTab := strings.Split(msg, ";")
	var err error
	var tab = make([]time.Duration, 4)
	for i := range messageTab {
		tab[i], err = time.ParseDuration(messageTab[i])
		if err != nil {
			log.Fatal("parse error :", err)
		}
	}
	return tab
}

// ParsePlayersSpeed : Parse player speeds from a message and return them in a slice
func (c ClientTool) ParsePlayersSpeed(msg string) []float64 {
	messageTab := strings.Split(msg, ";")
	var err error
	var tab = make([]float64, 4)
	for i := range messageTab {
		tab[i], err = strconv.ParseFloat(messageTab[i], 64)
		if err != nil {
			log.Fatal("parse error :", err)
		}
	}
	return tab
}

// Return the number of players currently connected
func (c ClientTool) GetNumberOfPlayerConnected(msg string) int {
	messageTab := strings.Split(msg, ";")
	return len(messageTab)
}
